@isTest
public class CustomDataloadControllerTest {

    @testSetup static void setup() {
        //setup existing product
        insert buildProduct('DELTALIGHT-9528-ED1-B-W', 'TEST_DELTALIGHT-9528-ED1-B-W', 'LUMINAIRE', '9528 ED1 B-W');
        //setup New Zealand Price Book
        insert buildPricebookByName('New Zealand Price Book');
        //setup product rule
        insert buildProductRule('Alert if no Components', 'Alert', 'Product', 'All', 
                                'No Component has been selected, are you sure you wish to continue', 'Always');
        
    }
    
    private static Product2 buildProduct(String productId, String name, String productType, String productCode) 
    {
        return new Product2(IsActive=true, ProductID__c=productId, Name=name, Product_Type__c=productType, ProductCode=productCode);
    }
    
    private static Dataload__c buildDataload(String productId, String productType, String productCode, Decimal tradePrice, 
                                             Decimal costPrice, Decimal nzTradePrice, Decimal nzCostPrice,
                                             String associatedComponents, String associatedOption, String associatedAccessories) 
    {                                  
        return new Dataload__c(ProductId__c=productId, Product_Type__c=productType, Product_Code__c=productCode, Trade_Price__c=tradePrice,
                               Cost_Price__c=costPrice, NZ_Trade_Price__c=nzTradePrice, NZ_Cost_Price__c=nzCostPrice,
                               Associated_Products_Components__c=associatedComponents, Associated_Products_Options__c=associatedOption,
                               Associated_Products_Accessories__c=associatedAccessories);
    }
    
    private static Pricebook2 buildPricebookByName(String name) 
    {
        return new Pricebook2(Name=name, IsActive=true);
    }
    
    private static SBQQ__ProductRule__c buildProductRule(String name, String ruleType, String scope, String condition, String message, STring event) 
    {
        return new SBQQ__ProductRule__c(Name=name, SBQQ__Type__c=ruleType, SBQQ__Active__c=true, SBQQ__Scope__c=scope, SBQQ__ConditionsMet__c=condition,
                                       SBQQ__ErrorMessage__c=message, SBQQ__EvaluationEvent__c=event);
    }
    
    private static SBQQ__ProductFeature__c buildProductFeature(String name, String productId) 
    {
        SBQQ__ProductFeature__c feature = new SBQQ__ProductFeature__c();
        feature.Name = name;
        feature.SBQQ__MinOptionCount__c = 0;
        feature.SBQQ__MaxOptionCount__c = 0;
        feature.SBQQ__Number__c = 1;
        feature.SBQQ__ConfiguredSKU__c = productId;
        
        return feature;
    }
    
    private static PricebookEntry buildPricebookEntry(String pricebook2Id, String product2Id) 
    {
        PricebookEntry pricebookEntry = new PricebookEntry();
        pricebookEntry.IsActive = true;
        pricebookEntry.Pricebook2Id = pricebook2Id;
        pricebookEntry.UnitPrice = 10;
        pricebookEntry.Product2Id = product2Id;
        
        return pricebookEntry;
    }
    
    private static SBQQ__Cost__c buildCost(String productId, String country) 
    {
        SBQQ__Cost__c cost = new SBQQ__Cost__c();
        cost.SBQQ__Active__c = true;
        cost.SBQQ__UnitCost__c = 10;
        cost.SBQQ__Product__c = productId;
        cost.Country__c = country;
        
        return cost;
    }
    
    private static SBQQ__ConfigurationRule__c buildConfigurationRule(String productRuleId, String product2Id) {
        SBQQ__ConfigurationRule__c config = new SBQQ__ConfigurationRule__c();
        config.SBQQ__Active__c = true;
        config.SBQQ__ProductRule__c = productRuleId;
        config.SBQQ__Product__c = product2Id;
        
        return config;
    }
    
    private static SBQQ__ProductOption__c buildProductOption(String mainProductId, String subProductId, String productFeatureId) {
        SBQQ__ProductOption__c option = new SBQQ__ProductOption__c();
        option.SBQQ__ConfiguredSKU__c = mainProductId;
        option.SBQQ__OptionalSKU__c = subProductId;
        option.SBQQ__Feature__c = productFeatureId;
        option.SBQQ__Number__c = 1;
        option.SBQQ__Type__c = 'Component';
        option.SBQQ__QuantityEditable__c = true;
        
        return option;
    }
    
    @isTest static void runDataloadTest()
    {
        List<Dataload__c> services = new List<Dataload__c>();
        Test.StartTest();
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(services);        
        CustomDataloadImportController ctrl = new CustomDataloadImportController(ssc);
        ctrl.runDataload();
        Test.StopTest();
    } 
    
    @isTest static void createNewDataloadRecordWithMatchedProductTest() 
    {
        
        Test.startTest();
        insert buildDataload('DELTALIGHT-9528-ED1-B-W', 'LUMINAIRE', '9528 ED1 B-W', 100.50, 60.00, 120.50, 70.00, null, null, null);
        Test.stopTest();
    }
    
    @isTest static void createNewDataloadRecordWithoutMatchedProductTest() 
    {
        Test.startTest();
        insert buildDataload('DELTALIGHT-1111-TST-B-K', 'LUMINAIRE', '1111 TST B-K', 100.50, 60.00, 120.50, 70.00, null, null, null);
        Test.stopTest();
    }
    
    @isTest static void executeCustomDataloadScheduleWithValidDataTest() 
    {
        List<Dataload__c> dataloadList = new List<Dataload__c>();
        dataloadList.add(buildDataload('DELTALIGHT-1111-TST-A-1', 'LUMINAIRE', '1111 TST B-K', 100.50, 60.00, 120.50, 70.00, 
                                       'DELTALIGHT-1112-TST-A', 'DELTALIGHT-1113-TST-A', 'DELTALIGHT-1114-TST-A'));
        dataloadList.add(buildDataload('DELTALIGHT-1112-TST-A', 'COMPONENT', '1111 TST B-K', 100.50, 60.00, 120.50, 70.00, null, null, null));
        dataloadList.add(buildDataload('DELTALIGHT-1113-TST-A', 'OPTION', '1111 TST B-K', 100.50, 60.00, 120.50, 70.00, null, null, null));
        dataloadList.add(buildDataload('DELTALIGHT-1114-TST-A', 'ACCESSORY', '1111 TST B-K', 100.50, 60.00, 120.50, 70.00, null, null, null));
        insert dataloadList;
        
        Test.startTest();
        String sch = '0 0 * * * ?';
        system.schedule('Test Custom Dataload Batch', sch, new CustomDataloadSchedule());
        Test.stopTest();
    }
    
    @isTest static void executeCustomDataloadScheduleWithInvalidDataTest() 
    {
        List<Dataload__c> dataloadList = new List<Dataload__c>();
        dataloadList.add(buildDataload('DELTALIGHT-1111-TST-A-1', 'LUMINAIRE', '1111 TST B-K', 100.50, 60.00, 120.50, 70.00, 
                                       'DELTALIGHT-1112-TST-A', 'DELTALIGHT-1113-TST-A', 'DELTALIGHT-1114-TST-A'));
        insert dataloadList;
        
        Test.startTest();
        String sch = '0 0 * * * ?';
        system.schedule('Test Custom Dataload Batch', sch, new CustomDataloadSchedule());
        Test.stopTest();
    }
    
    @isTest static void executeCustomDataloadScheduleWithExistingDataTest() 
    {
        Product2 mainProduct = [Select Id From Product2 Where ProductID__c = 'DELTALIGHT-9528-ED1-B-W' Limit 1];
        insert buildProductFeature('Components', mainProduct.Id);
        insert buildProductFeature('Options', mainProduct.Id);
        insert buildProductFeature('Accessories', mainProduct.Id);
        
        insert buildProduct('DELTALIGHT-1112-TST-A', 'DELTALIGHT-1112-TST-A', 'COMPONENT', 'component child product');
        insert buildProduct('DELTALIGHT-1113-TST-A', 'DELTALIGHT-1113-TST-A', 'OPTION', 'option child product');
        insert buildProduct('DELTALIGHT-1114-TST-A', 'DELTALIGHT-1114-TST-A', 'ACCESSORY', 'accessory child product');
        Product2 componentProduct = [Select Id From Product2 Where ProductID__c = 'DELTALIGHT-1112-TST-A' Limit 1];
        Product2 optionProduct = [Select Id From Product2 Where ProductID__c = 'DELTALIGHT-1113-TST-A' Limit 1];
        Product2 accessoryProduct = [Select Id From Product2 Where ProductID__c = 'DELTALIGHT-1114-TST-A' Limit 1];
        
        insert buildConfigurationRule([Select Id From SBQQ__ProductRule__c Where Name = 'Alert if no Components' Limit 1].Id, mainProduct.Id);
        
        insert buildCost(mainProduct.Id, 'AU');
        insert buildCost(mainProduct.Id, 'NZ');
		
        //insert standard pricebook
        insert buildPricebookEntry(Test.getStandardPricebookId(), mainProduct.Id);       
        insert buildPricebookEntry([Select Id From Pricebook2 Where Name = 'New Zealand Price Book' Limit 1].Id, mainProduct.Id);
        
        insert buildProductOption(mainProduct.Id, componentProduct.Id, [Select Id 
                                                                        From SBQQ__ProductFeature__c 
                                                                        Where SBQQ__ConfiguredSKU__c = :mainProduct.Id 
                                                                        And Name = 'Components'
                                                                        Limit 1].Id);
        insert buildProductOption(mainProduct.Id, componentProduct.Id, [Select Id 
                                                                        From SBQQ__ProductFeature__c 
                                                                        Where SBQQ__ConfiguredSKU__c = :mainProduct.Id 
                                                                        And Name = 'Options'
                                                                        Limit 1].Id);
        insert buildProductOption(mainProduct.Id, componentProduct.Id, [Select Id 
                                                                        From SBQQ__ProductFeature__c 
                                                                        Where SBQQ__ConfiguredSKU__c = :mainProduct.Id 
                                                                        And Name = 'Accessories'
                                                                        Limit 1].Id);
        
        List<Dataload__c> dataloadList = new List<Dataload__c>();
        dataloadList.add(buildDataload('DELTALIGHT-9528-ED1-B-W', 'LUMINAIRE', '1111 TST B-K', 100.50, 60.00, 120.50, 70.00, 
                                       'DELTALIGHT-1112-TST-A', 'DELTALIGHT-1113-TST-A', 'DELTALIGHT-1114-TST-A'));
        dataloadList.add(buildDataload('DELTALIGHT-1112-TST-A', 'COMPONENT', '1111 TST B-K', 100.50, 60.00, 120.50, 70.00, null, null, null));
        dataloadList.add(buildDataload('DELTALIGHT-1113-TST-A', 'OPTION', '1111 TST B-K', 100.50, 60.00, 120.50, 70.00, null, null, null));
        dataloadList.add(buildDataload('DELTALIGHT-1114-TST-A', 'ACCESSORY', '1111 TST B-K', 100.50, 60.00, 120.50, 70.00, null, null, null));
        insert dataloadList;
        
        Test.startTest();
        String sch = '0 0 * * * ?';
        system.schedule('Test Custom Dataload Batch', sch, new CustomDataloadSchedule());
        Test.stopTest();
    }
    
    
}
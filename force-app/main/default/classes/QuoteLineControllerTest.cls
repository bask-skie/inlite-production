@isTest
public class QuoteLineControllerTest {

    @testSetup static void setup() {
        
        Account acc = new Account();
        acc.Name = 'ACC TEST';
        acc.Account_Grade__c = '3';
        acc.Type = 'Specifier';
        acc.Specifier_Type__c = 'End User';
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'OPP TEST';
        opp.StageName = 'Quoting';
        opp.CloseDate = System.today() + 1;
        insert opp;

        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        quote.OwnerId =  UserInfo.getUserId();
        quote.SBQQ__Account__c = acc.Id;
        quote.SBQQ__Opportunity2__c = opp.Id;
        insert quote;
        
        SBQQ__QuoteLineGroup__c quoteGroup = new SBQQ__QuoteLineGroup__c();
        quoteGroup.Name = 'Group Test';
        quoteGroup.SBQQ__Number__c = 1;
        quoteGroup.SBQQ__Quote__c = quote.Id;
        quoteGroup.SBQQ__ListTotal__c = 10;
        quoteGroup.SBQQ__CustomerTotal__c = 10;
        quoteGroup.SBQQ__NetTotal__c = 10;
        insert quoteGroup;
        
        SBQQ__QuoteLineGroup__c quoteGroup2 = new SBQQ__QuoteLineGroup__c();
        quoteGroup2.Name = 'Group Test2';
        quoteGroup2.SBQQ__Number__c = 1;
        quoteGroup2.SBQQ__Quote__c = quote.Id;
        quoteGroup2.SBQQ__ListTotal__c = 10;
        quoteGroup2.SBQQ__CustomerTotal__c = 10;
        quoteGroup2.SBQQ__NetTotal__c = 10;
        insert quoteGroup2;
        
        Product2 prod = new Product2();
        prod.Name = 'Product Test';
        insert prod;
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
		ql.SBQQ__Quote__c = quote.Id;
        ql.SBQQ__Group__c = quoteGroup.Id;
        ql.SBQQ__Product__c = prod.Id;
        insert ql;
    }
    
    @isTest static void processCreatedWithValidDataTest() {
        Test.startTest();
        
		List<SBQQ__QuoteLine__c> newQuoteLineList= [Select Id, Read_Only_Quote_Line__c From SBQQ__QuoteLine__c Limit 1];
		List<SBQQ__QuoteLineGroup__c> newQuoteGroupList = [Select Id From SBQQ__QuoteLineGroup__c 
                                                           Where Name = 'Group Test2' 
                                                           Limit 1];
        
        newQuoteLineList.get(0).SBQQ__Group__c = newQuoteGroupList.get(0).Id;
		update newQuoteLineList;
        
        newQuoteLineList.get(0).SBQQ__Group__c = null;
        update newQuoteLineList;
            
        delete newQuoteLineList;
        Test.stopTest();
    }    
    
}
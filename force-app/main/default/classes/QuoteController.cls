public class QuoteController {
    public static void createQuote(List<SBQQ__Quote__c> quoteList){
        
        List<Read_Only_Quote__c> readOnlyQuoteList = new List<Read_Only_Quote__c>();
        Map<String, Read_Only_Quote__c> readOnlyQuoteMap = new Map<String, Read_Only_Quote__c>();
        
        for(SBQQ__Quote__c quote: quoteList){
        	Read_Only_Quote__c readOnlyQuote = new Read_Only_Quote__c();
            readOnlyQuote.Quote__c = quote.Id;
        	readOnlyQuote.OwnerId = quote.OwnerId;
            readOnlyQuote.Account__c = quote.SBQQ__Account__c;
            readOnlyQuote.Opportunity__c = quote.SBQQ__Opportunity2__c;
            readOnlyQuoteList.add(readOnlyQuote);
            readOnlyQuoteMap.put(quote.Id, readOnlyQuote);
       }

        if(readOnlyQuoteList.size() > 0){
            insert readOnlyQuoteList;
            
            List<SBQQ__Quote__c> newQuoteList= [Select Id, OwnerId, SBQQ__Account__c, SBQQ__Opportunity2__c,
                                                Read_Only_Quote__c
                                                From SBQQ__Quote__c
                                                Where Id In : quoteList];

            for(SBQQ__Quote__c quote: newQuoteList){
                if(readOnlyQuoteMap.get(quote.Id) != null){
                    quote.Read_Only_Quote__c = readOnlyQuoteMap.get(quote.Id).Id;
                }
            }
            
            if(newQuoteList.size() > 0) {
                update newQuoteList;
            }
        }
    }
    
    public static void deleteQuote(List<SBQQ__Quote__c> quoteList){
        Set<String> readOnlyQuoteId = new Set<String>();
        for(SBQQ__Quote__c quote: quoteList){
            if(quote.Read_Only_Quote__c != null){
            	readOnlyQuoteId.add(quote.Read_Only_Quote__c);
            }
        }
        List<Read_Only_Quote__c> readOnlyQuoteList = [Select Id From Read_Only_Quote__c Where Id in: readOnlyQuoteId];
        if(readOnlyQuoteList.size() > 0) {
            delete readOnlyQuoteList;
        }
    } 
    
    public static void updateQuote(List<SBQQ__Quote__c> quoteList){
        Set<String> readOnlyQuoteId = new Set<String>();
        Map<String, SBQQ__Quote__c> quoteMap = new Map<String, SBQQ__Quote__c>();
        for(SBQQ__Quote__c quote: quoteList){
            readOnlyQuoteId.add(quote.Read_Only_Quote__c);
            quoteMap.put(quote.Id, quote);
        }
        
        List<Read_Only_Quote__c> readOnlyQuoteList = [Select Id, Quote__c From Read_Only_Quote__c Where Id in: readOnlyQuoteId];
        List<Read_Only_Quote__c> updateReadOnlyQuoteList = new List<Read_Only_Quote__c>();
        for(Read_Only_Quote__c readOnlyQuote : readOnlyQuoteList){
            readOnlyQuote.Account__c = ((SBQQ__Quote__c)quoteMap.get(readOnlyQuote.Quote__c)).SBQQ__Account__c;
            readOnlyQuote.Opportunity__c = ((SBQQ__Quote__c)quoteMap.get(readOnlyQuote.Quote__c)).SBQQ__Opportunity2__c;
            updateReadOnlyQuoteList.add(readOnlyQuote);
        }
        if(updateReadOnlyQuoteList.size() > 0) {
            update updateReadOnlyQuoteList;
        }
    }
}
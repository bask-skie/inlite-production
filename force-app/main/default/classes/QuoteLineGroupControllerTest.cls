@isTest
public class QuoteLineGroupControllerTest {
  @testSetup static void setup() {
        
		Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
         
        User usr = new User(LastName = 'L-TEST',
                           FirstName='F-TEST',
                           Alias = 'TEST',
                           Email = 'quote-test@gmail.com',
                           Username = 'quote-test@gmail.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                          LocaleSidKey = 'en_US'
                           );
        insert usr;
        
        Account acc = new Account();
        acc.Name = 'ACC TEST';
        acc.Account_Grade__c = '3';
        acc.Type = 'Specifier';
        acc.Specifier_Type__c = 'End User';
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'OPP TEST';
        opp.StageName = 'Quoting';
        opp.CloseDate = System.today() + 1;
        insert opp;

        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        quote.OwnerId = usr.Id;
        quote.SBQQ__Account__c = acc.Id;
        quote.SBQQ__Opportunity2__c = opp.Id;
        insert quote;
    }
    
    @isTest static void processCreatedWithValidDataTest() {
        Test.startTest();
        List<SBQQ__Quote__c> newQuoteList= [Select Id, Read_Only_Quote__c From SBQQ__Quote__c];
		SBQQ__QuoteLineGroup__c qg = new SBQQ__QuoteLineGroup__c();
		qg.Name = 'GROUP TEST';
		qg.SBQQ__Quote__c = newQuoteList.get(0).Id;
		qg.SBQQ__Number__c = 1;
		qg.SBQQ__ListTotal__c = 1;
		qg.SBQQ__CustomerTotal__c = 1;
		qg.SBQQ__NetTotal__c = 1;
		insert qg;
        
        delete qg;
        Test.stopTest();
    }    
}
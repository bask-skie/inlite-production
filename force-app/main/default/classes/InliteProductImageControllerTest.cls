@isTest
public class InliteProductImageControllerTest {

    @testSetup static void setup() {
    	createCustomSetting();
    	createProductRecord();
    }
    
    private static void createCustomSetting() {
        Product_Image_Setting__c setting = new Product_Image_Setting__c();
        setting.API_Key__c = 'test-0123';
        setting.Product_Detail_Endpoint__c = 'http://example.com/example/test';
        
        insert setting; 
    }
    
    private static void createProductRecord() {
        Product2 product = new Product2();
        product.IsActive = true;
        product.ProductID__c = 'TEST-9528-ED1-B-W';
        product.Name = 'TEST_01-9528-ED1-B-W';
        product.ProductCode = '9528 ED1 B-W';
        product.Images_Retrieved__c = false;
        
        insert product;
    }
    
    @isTest static void getInliteImageUrlArrayWithSuccessResponseTest() {
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockInliteProductImageResponseGenerator(true, 200));
        String sch = '0 0 * * * ?';
        system.schedule('Test Inlite Image API Batch', sch, new InliteProductImageSchedule());
        Test.stopTest();
    }
    
    @isTest static void getInliteImageUrlObjectWithSuccessResponseTest() {
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockInliteProductImageResponseGenerator(false, 200));
        String sch = '0 0 * * * ?';
        system.schedule('Test Inlite Image API Batch', sch, new InliteProductImageSchedule());
        Test.stopTest();
    }
    
    @isTest static void getInliteImageUrlArrayWithCalloutRetryTest() {
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockInliteProductImageResponseGenerator(true, 302));
        String sch = '0 0 * * * ?';
        system.schedule('Test Inlite Image API Batch', sch, new InliteProductImageSchedule());
        Test.stopTest();
    }
    
    @isTest static void getInliteImageUrlWithFailedResponseTest() {
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockInliteProductImageResponseGenerator(true, 500));
        String sch = '0 0 * * * ?';
        system.schedule('Test Inlite Image API Batch', sch, new InliteProductImageSchedule());
        Test.stopTest();
    }
    
}
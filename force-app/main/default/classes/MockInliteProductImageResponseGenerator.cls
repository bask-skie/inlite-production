@isTest
global class MockInliteProductImageResponseGenerator implements HttpCalloutMock {

    private Boolean IsSuccessed = true;
    private Boolean IsArray = true;
    private Integer statusCode = 200;
    
    global MockInliteProductImageResponseGenerator(){
    }
    
    global MockInliteProductImageResponseGenerator(Boolean isArray, Integer statusCode){
        this.IsArray = isArray;
        this.statusCode = statusCode;
    }
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://example.com/example/test', req.getEndpoint());
        System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        String bodyObj = '{"inlite_code":"TEST-9528-ED1-B-W",'+
            			 '"application_images": ['+
            				 '{"sizes": {'+
            					 		'"medium": "http://example.com/example/test/product/images/APP-image-test-0001.jpg"}'+
                             '},'+
                             '{"sizes": {'+
                             	'"medium": "http://example.com/example/test/product/images/APP-image-test-0002.jpg"}'+
                             '},'+
                             '{"sizes": {'+
                             	'"medium": "http://example.com/example/test/product/images/APP-image-test-0003.jpg"}'+
                             '},'+
                             '{"sizes": {'+
                             	'"medium": "http://example.com/example/test/product/images/APP-image-test-0004.jpg"}'+
                             '},'+
                             '{"sizes": {'+
                             	'"medium": "http://example.com/example/test/product/images/APP-image-test-0005.jpg"}'+
                             '},'+
                             '{"sizes": {'+
                             	'"medium": "http://example.com/example/test/product/images/APP-image-test-0006.jpg"}'+
                             '},'+
                             '{"sizes": {'+
                             	'"medium": "http://example.com/example/test/product/images/APP-image-test-0006.jpg"}'+
                             '}'+
            			 '],'+
            			 '"product_hero": {'+
            			 	 '"medium": "http://example.com/example/test/product/images/Hero-test-0001.jpg"'+
            			 '}'+
            		    '}';
        String bodyArray = '{"products":['+ bodyObj + ']}';
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(statusCode);
        if(statusCode == 302){
            res.setHeader('Location', 'www.test.com');
        }

        if(IsArray) {
            res.setBody(bodyArray);
        }else {
            res.setBody(bodyObj);
        }
        
        return res;
    }
}
public class CustomDataloadImportController {
    
    public CustomDataloadImportController(ApexPages.StandardSetController controller) {
        
    }
    
    public PageReference runDataload()
    {
        List<AsyncApexJob> jobs = [SELECT Id, CreatedDate 
                                   FROM AsyncApexJob 
                                   WHERE (ApexClass.Name = 'CustomDataloadBatch' 
                                          OR ApexClass.Name = 'CustomDataloadController') 
                                   AND Status = 'Processing'];        
        if (jobs.size() > 0)
        {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Import Job is already running.')); return null;
        }
		else
        {
            //MarketingCloudDataloadBatch.runDataload('Dataload_Customer__c', true);
            Database.executeBatch(new CustomDataloadBatch(), 5);
            return new ApexPages.Action('{!List}').invoke();            
        }
    }

}
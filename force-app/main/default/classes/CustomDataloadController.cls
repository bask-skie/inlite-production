public class CustomDataloadController {
    
    private static final String COMPONENT = 'Components';
    private static final String OPTION = 'Options';
    private static final String ACCESSORY = 'Accessories';
    private static final String PRODUCT_RULE_NAME = 'Alert if no Components';
    private static final String NEW_ZEALAND_PRICE_BOOK = 'New Zealand Price Book';
    private static final String AU_PREFIX = 'AU_';
    private static final String NZ_PREFIX = 'NZ_';
    private static final String COMPONENT_PREFIX = 'COMPONENT_';
    private static final String OPTION_PREFIX = 'OPTION_';
    private static final String ACCESSORY_PREFIX = 'ACCESSORY_';

    public static void populateExistingProducts(List<Dataload__c> newDataloadList){
        Set<String> productIdSet = new Set<String>();
        Map<String, Product2> productMap = new Map<String, Product2>();
        
        //setup data
        for(Dataload__c dataload: newDataloadList) {
            if(dataload.ProductID__c != null) {
                productIdSet.add(dataload.ProductID__c);
            }
        }
        
        //given data 
        List<Product2> products = [Select Id, ProductID__c From Product2 Where ProductID__c In: productIdSet];
        for(Product2 product: products) {
            if(product.ProductID__c != null) {
                productMap.put(product.ProductID__c, product);
            }
        }
        
        //execute process for checking existing product
        for(Dataload__c dataload: newDataloadList) {
            Product2 product = productMap.get(dataload.ProductID__c);
            if(product != null) {
                dataload.Product__c = product.Id;
                dataload.Existing_Product__c = true;
            }else {
                dataload.No_Existing_Product__c = true;
            }
        }
    }
    
    public static void createNewProducts(List<Dataload__c> newDataloadList) {
        List<Product2> newProducts = new List<Product2>();
        List<Product2> updProducts = new List<Product2>();
        Map<String , Product2> newProductMap = new Map<String, Product2>();
        //Map data to create a new Product
        for(Dataload__c dataload: newDataloadList) {
            System.debug('ProductId__c: '+dataload.ProductId__c);
            if(dataload.No_Existing_Product__c && dataload.ProductId__c != null) 
            {
                Product2 product = mapProductData(dataload);
                
                newProducts.add(product);
            } 
            else if(dataload.Existing_Product__c && dataload.Product__c != null) 
            {
                Product2 product = mapProductData(dataload);
                product.Id = dataload.Product__c;
                
                updProducts.add(product);
            }
        }
        
        if(newProducts.size() > 0) 
        {
            Database.insert(newProducts, false);
        }
        if(updProducts.size() > 0) 
        {
            Database.update(updProducts, false);
        }
        
    }
    
    public static Product2 mapProductData(Dataload__c dataload) {
        Product2 product = new Product2();
        product.IsActive = true;
        if(dataload.ProductId__c != null) {
            product.ProductID__c = dataload.ProductId__c;
            product.Name = dataload.ProductId__c;
        }
        if(dataload.Product_Code__c != null) product.ProductCode = dataload.Product_Code__c;
        if(dataload.Product_Type__c != null) product.Product_Type__c = dataload.Product_Type__c;
        if(dataload.Family_Name__c != null) product.Family_Name__c = dataload.Family_Name__c;
        if(dataload.Product_Description__c != null) product.Description = dataload.Product_Description__c;
        if(dataload.IP_Rating__c != null) product.IP_Rating__c = dataload.IP_Rating__c;
        if(dataload.IK_Rating__c != null) product.IK_Rating__c = dataload.IK_Rating__c;
        if(dataload.Light_Source__c != null) product.Light_Source__c = dataload.Light_Source__c;
        if(dataload.Light_Source_Base__c != null) product.Light_Source_Base__c = dataload.Light_Source_Base__c;
        if(dataload.Voltage__c != null) product.Voltage__c = dataload.Voltage__c;
        if(dataload.Current__c != null) product.Current__c = dataload.Current__c;
        if(dataload.Wattage__c != null) product.Wattage__c = dataload.Wattage__c;
        if(dataload.System_Power__c != null) product.System_Power__c = dataload.System_Power__c;
        if(dataload.Lumen_Package__c != null) product.Lumen_Package__c = dataload.Lumen_Package__c;
        if(dataload.Luminous_Flux__c != null) product.Luminous_Flux__c = dataload.Luminous_Flux__c;
        if(dataload.Colour_Temperature__c != null) product.Colour_Temp__c = dataload.Colour_Temperature__c;
        if(dataload.CRI__c != null) product.CRI__c = dataload.CRI__c;
        if(dataload.Chromacity_Tolerance__c != null) product.Chromacity_Tolerance__c = dataload.Chromacity_Tolerance__c;
        if(dataload.Beam_Angle__c != null) product.Beam_Angle__c = dataload.Beam_Angle__c;
        if(dataload.Distribution_Type__c != null) product.Distribution_Type__c = dataload.Distribution_Type__c;
        if(dataload.Control_Gear__c != null) product.Control_Gear__c = dataload.Control_Gear__c;
        if(dataload.Control_Gear_Detail__c != null) product.Control_Gear_Detail__c = dataload.Control_Gear_Detail__c;
        if(dataload.Product_Application_Type__c != null) product.Product_Application_Type__c = dataload.Product_Application_Type__c;
        if(dataload.Application_Type__c != null) product.Application_Type__c = dataload.Application_Type__c;
        if(dataload.Manufacturer__c != null) product.Brands__c = dataload.Manufacturer__c;
        
        product.SBQQ__ConfigurationType__c = 'Allowed';
        product.SBQQ__ConfigurationEvent__c = 'Always';
        product.SBQQ__OptionLayout__c = 'Sections';
        product.SBQQ__OptionSelectionMethod__c = 'Click';
        product.SBQQ__PriceEditable__c = true;
        product.SBQQ__PricingMethod__c = 'Cost';
        product.SBQQ__PricingMethodEditable__c = true;
        
        return product;
    }
    
    public static void manageCustomDataload(List<Dataload__c> dataloadList) {
        
        List<PricebookEntry> auPricebookEntryList = new List<PricebookEntry>();
        List<PricebookEntry> nzPricebookEntryList = new List<PricebookEntry>();
        
        //new
        List<PricebookEntry> newPricebookEntryList = new List<PricebookEntry>();
        List<PricebookEntry> updPricebookEntryList = new List<PricebookEntry>();
        List<SBQQ__Cost__c> newCostList = new List<SBQQ__Cost__c>();
        List<SBQQ__Cost__c> updCostList = new List<SBQQ__Cost__c>();
        
        List<SBQQ__Cost__c> auCostList = new List<SBQQ__Cost__c>();
        List<SBQQ__Cost__c> nzCostList = new List<SBQQ__Cost__c>();
        List<SBQQ__ProductFeature__c> productFeatureList = new List<SBQQ__ProductFeature__c>();
        List<SBQQ__ConfigurationRule__c> configurationRuleList = new List<SBQQ__ConfigurationRule__c>();
        
        List<SBQQ__ProductOption__c> componentProductOptionList = new List<SBQQ__ProductOption__c>();
        List<SBQQ__ProductOption__c> optionProductOptionList= new List<SBQQ__ProductOption__c>();
        List<SBQQ__ProductOption__c> accessoryProductOptionList = new List<SBQQ__ProductOption__c>();
        
        Set<String> productIdSet = new Set<String>();
        Set<String> productComponentIdSet = new Set<String>();
        Set<String> productOptionIdSet = new Set<String>();
        Set<String> productAccessoryIdSet = new Set<String>();
        Set<String> childProductIdSet = new Set<String>();
        
        Map<String, Product2> productMap = new Map<String, Product2>();
        Map<String, Product2> productComponentMap = new Map<String, Product2>();
        Map<String, Product2> productOptionMap = new Map<String, Product2>();
        Map<String, Product2> productAccessoryMap = new Map<String, Product2>();
        
        Id stdPricebookId = CustomDataloadAssembler.getStandardPricebookId();
        Id nzPricebookId = CustomDataloadAssembler.getPricebookIdByName(NEW_ZEALAND_PRICE_BOOK);
        Id productRuleId = CustomDataloadAssembler.getProductRuleIdByName(PRODUCT_RULE_NAME);
        
        
        for(Dataload__c dataload: dataloadList) {
            //given productId for MAP
            if(dataload.ProductId__c != null) {
                productIdSet.add(dataload.ProductId__c);
            }
            
            if(dataload.Associated_Products_Components__c != null) {
                String[] associatedComponents = ((String) dataload.Associated_Products_Components__c).split(',');
                for(String productId: associatedComponents) 
                {
                    productComponentIdSet.add(productId.trim());
                }
            }
            
            if(dataload.Associated_Products_Options__c != null) {
                String[] associatedOptions = ((String) dataload.Associated_Products_Options__c).split(',');
                for(String productId: associatedOptions) 
                {
                    productOptionIdSet.add(productId.trim());
                }
            }
            
            if(dataload.Associated_Products_Accessories__c != null) {
                String[] associatedAccessories = ((String) dataload.Associated_Products_Accessories__c).split(',');
                for(String productId: associatedAccessories) 
                {
                    productAccessoryIdSet.add(productId.trim());
                }
            }
            
        }
        
        Set<String> productRecordIdSet = new Set<String>();
        for(Product2 product: [SELECT Id, ProductID__c, Product_Type__c
                               From Product2 
                               Where ProductID__c In: productIdSet]){
            productMap.put(product.ProductID__c, product);
            productRecordIdSet.add(product.Id);                       
        }
        
        for(Product2 productComponent: [SELECT Id, ProductID__c, Product_Type__c 
                                        From Product2 
                                        Where ProductID__c In: productComponentIdSet]) {
            productComponentMap.put(productComponent.ProductID__c, productComponent);
        }
        
        for(Product2 productOption: [SELECT Id, ProductID__c, Product_Type__c 
                                     From Product2 
                                     Where ProductID__c In: productOptionIdSet]) {
            productOptionMap.put(productOption.ProductID__c, productOption);
        }
        
        for(Product2 productAccessory: [SELECT Id, ProductID__c, Product_Type__c 
                                        From Product2 
                                        Where ProductID__c In: productAccessoryIdSet]) {
            productAccessoryMap.put(productAccessory.ProductID__c, productAccessory);
        }
        
        //NEW CHANGE
        //Given existing AU&NZ List Prices
        Map<String, PricebookEntry> existingPricebookEntryMap = new Map<String, PricebookEntry>();
        List<PricebookEntry> existingPricebookEntryList = [Select Id, Pricebook2Id, Product2Id, UnitPrice 
                                                             From PricebookEntry 
                                                             Where Product2Id In :productRecordIdSet];
        System.debug('existingPricebookEntryList: '+existingPricebookEntryList);
        for(PricebookEntry pricebookEntry : existingPricebookEntryList) {
            if(pricebookEntry.Pricebook2Id == stdPricebookId) {
                if(existingPricebookEntryMap.get(AU_PREFIX + pricebookEntry.Product2Id) == null) {
                    existingPricebookEntryMap.put(AU_PREFIX + pricebookEntry.Product2Id, pricebookEntry);
                }
            }else if(pricebookEntry.Pricebook2Id == nzPricebookId) {
                if(existingPricebookEntryMap.get(NZ_PREFIX + pricebookEntry.Product2Id) == null) {
                    existingPricebookEntryMap.put(NZ_PREFIX + pricebookEntry.Product2Id, pricebookEntry);
                }
            }
        }
        System.debug('existingPricebookEntryMap: '+existingPricebookEntryMap);
        System.debug('existingPricebookEntryMap: '+existingPricebookEntryMap);
        
        Map<String, SBQQ__Cost__c> existingCostsMap = new Map<String, SBQQ__Cost__c>();
        List<SBQQ__Cost__c> existingCostList = [Select Id, SBQQ__UnitCost__c, Country__c, SBQQ__Product__c 
                                                 From SBQQ__Cost__c 
                                                 Where SBQQ__Product__c In :productRecordIdSet];
        for(SBQQ__Cost__c cost : existingCostList) {
            if(cost.Country__c == 'AU') {
                if(existingCostsMap.get(AU_PREFIX + cost.SBQQ__Product__c) == null) {
                    existingCostsMap.put(AU_PREFIX + cost.SBQQ__Product__c, cost);
                }
            }else if(cost.Country__c == 'NZ') {
                if(existingCostsMap.get(NZ_PREFIX + cost.SBQQ__Product__c) == null) {
                    existingCostsMap.put(NZ_PREFIX + cost.SBQQ__Product__c, cost);
                }
            }
        }
        
        Map<String, List<SBQQ__ProductOption__c>> productOptionMapByProductId = new Map<String, List<SBQQ__ProductOption__c>>();
        List<SBQQ__ProductOption__c> productOptionList = [Select Id, SBQQ__ConfiguredSKU__c, 
                                                          SBQQ__OptionalSKU__c, SBQQ__OptionalSKU__r.ProductID__c 
                                                          From SBQQ__ProductOption__c 
                                                          Where SBQQ__ConfiguredSKU__c In :productRecordIdSet];
        for(SBQQ__ProductOption__c option : productOptionList) {
            List<SBQQ__ProductOption__c> productOptionTmpList = productOptionMapByProductId.get(option.SBQQ__ConfiguredSKU__c);
            if(productOptionTmpList == null) 
            {
                productOptionMapByProductId.put(option.SBQQ__ConfiguredSKU__c, new List<SBQQ__ProductOption__c>{option});
            }
            else
            {
                productOptionTmpList.add(option);
            }
        }
        
        Map<String, SBQQ__ProductFeature__c> existingProductFeatureMap = new Map<String, SBQQ__ProductFeature__c>();
        List<SBQQ__ProductFeature__c> existingProductFeatureList = [Select Id, Name, SBQQ__ConfiguredSKU__c, SBQQ__ConfiguredSKU__r.ProductID__c
                                                                    From SBQQ__ProductFeature__c
                                                                    Where SBQQ__ConfiguredSKU__c In :productRecordIdSet];
        for(SBQQ__ProductFeature__c feature: existingProductFeatureList) 
        {
            if(feature.Name != null && feature.SBQQ__ConfiguredSKU__r.ProductID__c != null && 
               existingProductFeatureMap.get(feature.Name +''+ feature.SBQQ__ConfiguredSKU__r.ProductID__c) == null) 
            {
                existingProductFeatureMap.put(feature.Name +''+ feature.SBQQ__ConfiguredSKU__r.ProductID__c, feature);
            }
        }
        
        Map<String, SBQQ__ConfigurationRule__c> existingProductRuleMap = new Map<String, SBQQ__ConfigurationRule__c>();
        List<SBQQ__ConfigurationRule__c> existingProductRuleList = [Select Id, SBQQ__Product__c, SBQQ__Product__r.ProductID__c 
                                                                    From SBQQ__ConfigurationRule__c 
                                                                    Where SBQQ__Product__c In :productRecordIdSet];
        for(SBQQ__ConfigurationRule__c rule: existingProductRuleList) 
        {
            if(rule.SBQQ__Product__r.ProductID__c != null && existingProductFeatureMap.get(rule.SBQQ__Product__r.ProductID__c) == null) 
            {
                existingProductRuleMap.put(rule.SBQQ__Product__r.ProductID__c, rule);
            }
        }
        
        
        for(Dataload__c dataload: dataloadList) {
            Product2 product = productMap.get(dataload.ProductID__c);
            if(product != null) {
                
                dataload.Product__c = product.Id;
                
                if(dataload.Trade_Price__c != null && stdPricebookId != null) {
                    //auPricebookEntryList.add(CustomDataloadAssembler.toBuildPricebookEntry(stdPricebookId, 
                    //                                                                       dataload.Trade_Price__c, product.Id));
                    
                    System.debug('AU_PREFIX: '+AU_PREFIX);
                    System.debug('product.Id: '+product.Id);
                    System.debug('existingPricebookEntryMap.get(AU_PREFIX + product.Id): '+existingPricebookEntryMap.get(AU_PREFIX + product.Id));
                    if(existingPricebookEntryMap.get(AU_PREFIX + product.Id) != null) 
                    {
                        PricebookEntry pricebook = CustomDataloadAssembler.toBuildPricebookEntry(stdPricebookId, dataload.Trade_Price__c, product.Id);
                        pricebook.Id = existingPricebookEntryMap.get(AU_PREFIX + product.Id).Id;
                        updPricebookEntryList.add(pricebook);
                        
                    }
                    else 
                    {
                        newPricebookEntryList.add(CustomDataloadAssembler.toBuildPricebookEntry(stdPricebookId, dataload.Trade_Price__c, product.Id));
                    }
                    
                }
                
                if(dataload.NZ_Trade_Price__c != null && nzPricebookId != null) {
                    //nzPricebookEntryList.add(CustomDataloadAssembler.toBuildPricebookEntry(nzPricebookId, 
                    //                                                                       dataload.NZ_Trade_Price__c, product.Id));
                                                                                          
                    if(existingPricebookEntryMap.get(NZ_PREFIX + product.Id) != null) 
                    {
                        PricebookEntry pricebook = CustomDataloadAssembler.toBuildPricebookEntry(nzPricebookId, dataload.NZ_Trade_Price__c, product.Id);
                        pricebook.Id = existingPricebookEntryMap.get(NZ_PREFIX + product.Id).Id;
                        updPricebookEntryList.add(pricebook);
                    }
                    else
                    {
                        newPricebookEntryList.add(CustomDataloadAssembler.toBuildPricebookEntry(nzPricebookId, dataload.NZ_Trade_Price__c, product.Id));
                    }
                    
                }
                
                if(dataload.Cost_Price__c != null && stdPricebookId != null) {
                    //auCostList.add(CustomDataloadAssembler.toBuildCost(dataload.Cost_Price__c, product.Id, 'AU'));
                    if(existingCostsMap.get(AU_PREFIX + product.Id) != null) 
                    {
                        SBQQ__Cost__c cost = CustomDataloadAssembler.toBuildCost(dataload.Cost_Price__c, product.Id, 'AU');
                        cost.Id = existingCostsMap.get(AU_PREFIX + product.Id).Id;
                        updCostList.add(cost);
                    }
                    else 
                    {
                        newCostList.add(CustomDataloadAssembler.toBuildCost(dataload.Cost_Price__c, product.Id, 'AU'));
                    }
                }
                
                if(dataload.NZ_Cost_Price__c != null && nzPricebookId != null) {
                    //nzCostList.add(CustomDataloadAssembler.toBuildCost(dataload.NZ_Cost_Price__c, product.Id, 'NZ'));
                    if(existingCostsMap.get(NZ_PREFIX + product.Id) != null) 
                    {
                        SBQQ__Cost__c cost = CustomDataloadAssembler.toBuildCost(dataload.NZ_Cost_Price__c, product.Id, 'NZ');
                        cost.Id = existingCostsMap.get(NZ_PREFIX + product.Id).Id;
                        updCostList.add(cost);
                    }
                    else
                    {
                        newCostList.add(CustomDataloadAssembler.toBuildCost(dataload.NZ_Cost_Price__c, product.Id, 'NZ'));
                    }
                }
                
                //creating product_feaure for main product only
                if(dataload.Product_Type__c != 'ACCESSORY' && dataload.Product_Type__c != 'COMPONENT' && dataload.Product_Type__c != 'OPTION') {
                	
                    if(existingProductFeatureMap.get(COMPONENT +''+ dataload.ProductID__c) == null) 
                    {
                        productFeatureList.add(CustomDataloadAssembler.toBuildProductFeature(COMPONENT, 0, null, 1, product.Id));
                    }
                    	
                    if(existingProductFeatureMap.get(OPTION +''+ dataload.ProductID__c) == null) 
                    {
                        productFeatureList.add(CustomDataloadAssembler.toBuildProductFeature(OPTION, 0, null, 2, product.Id));
                    }
                    	
                    if(existingProductFeatureMap.get(ACCESSORY +''+ dataload.ProductID__c) == null) 
                    {
                        productFeatureList.add(CustomDataloadAssembler.toBuildProductFeature(ACCESSORY, 0, null, 3, product.Id));
                    }
                    	
                    //create product rule
                    if(productRuleId != null && existingProductRuleMap.get(dataload.ProductID__c) == null) 
                    {
                        configurationRuleList.add(CustomDataloadAssembler.toBuildConfigurationRule(productRuleId, product.Id));
                    }
                }
                
            }//end if prod-id != null
        }
        
        Map<String, PricebookEntry> successedPricebookEntryMap = new Map<String, PricebookEntry>();
        Map<String, SBQQ__Cost__c> successedCostMap = new Map<String, SBQQ__Cost__c>();
        Map<String, SBQQ__ProductFeature__c> successedProductFeatureMap = new Map<String, SBQQ__ProductFeature__c>();
        
        if(newPricebookEntryList.size() > 0) 
        {
            Database.insert(newPricebookEntryList, false);
            
            successedPricebookEntryMap = CustomDataloadAssembler.toBuildSuccessedPricebookEntryMap(stdPricebookId, nzPricebookId, AU_PREFIX, NZ_PREFIX,
                                                                                                    newPricebookEntryList, successedPricebookEntryMap);
        }
        if(updPricebookEntryList.size() > 0) 
        {
            Database.update(updPricebookEntryList, false);
            
            successedPricebookEntryMap = CustomDataloadAssembler.toBuildSuccessedPricebookEntryMap(stdPricebookId, nzPricebookId, AU_PREFIX, NZ_PREFIX,
                                                                                                    updPricebookEntryList, successedPricebookEntryMap);
        }
        
        if(newCostList.size() > 0) 
        {
            Database.insert(newCostList, false);
            
            successedCostMap = CustomDataloadAssembler.toBuildSuccessedCostMap(AU_PREFIX, NZ_PREFIX, newCostList, successedCostMap);
        }
        if(updCostList.size() > 0) 
        {
            Database.update(updCostList, false);
            
            successedCostMap = CustomDataloadAssembler.toBuildSuccessedCostMap(AU_PREFIX, NZ_PREFIX, newCostList, successedCostMap);
        }
        
        if(productFeatureList.size() > 0) 
        {
            Database.insert(productFeatureList, false);
        }
        //add existing product feature in case new product feature is not created
        if(existingProductFeatureList.size() > 0) {
            productFeatureList.addAll(existingProductFeatureList);
        }
        CustomDataloadAction.setupSuccessedProductFeatureMap(productFeatureList, successedProductFeatureMap,
                                                                COMPONENT_PREFIX, OPTION_PREFIX, ACCESSORY_PREFIX,
                                                                COMPONENT, OPTION, ACCESSORY);  
        
        if(configurationRuleList.size() > 0) 
        {
            Database.insert(configurationRuleList, false);
        }
        
        //set flag
        CustomDataloadAction.setupSuccessedFlag(dataloadList, productMap, successedPricebookEntryMap, 
                                              successedCostMap, AU_PREFIX, NZ_PREFIX);
        
        System.debug('dataloadList: '+dataloadList);
        //controll related product
        for(Dataload__c dataload: dataloadList) {
            Product2 product = productMap.get(dataload.ProductID__c);
            System.debug('product: '+product + ' flag: '+dataload.IsProcessed__c);
            //manage data for IsProcessed == false
            
            if(product != null && dataload.IsProcessed__c != true) {
                //creating the product_option record with associated product data
                if(dataload.Associated_Products_Components__c != null) {
                    System.debug('Associated_Products_Components__c != null');
                    Map<String, SBQQ__ProductOption__c> existingOptionMap = CustomDataloadAssembler.toBuildProductOptionMap(productOptionMapByProductId.get(product.Id));
                    String[] associatedComponents = ((String) dataload.Associated_Products_Components__c).split(',');
                    
                    CustomDataloadAction.addProductOptionList(dataload, associatedComponents, productComponentMap,
                                                              successedProductFeatureMap, componentProductOptionList,
                                                              COMPONENT_PREFIX, product, COMPONENT, existingOptionMap);
                }
                //creating the product_option record with associated product data 
                if(dataload.Associated_Products_Options__c != null) {
                    System.debug('Associated_Products_Options__c != null');
                    Map<String, SBQQ__ProductOption__c> existingOptionMap = CustomDataloadAssembler.toBuildProductOptionMap(productOptionMapByProductId.get(product.Id));
                    String[] associatedOptions = ((String) dataload.Associated_Products_Options__c).split(',');
                    
                    CustomDataloadAction.addProductOptionList(dataload, associatedOptions, productOptionMap,
                                                              successedProductFeatureMap, optionProductOptionList,
                                                              OPTION_PREFIX, product, OPTION, existingOptionMap);
                }
                //creating the product_option record with associated product data
                if(dataload.Associated_Products_Accessories__c != null) {
                    System.debug('Associated_Products_Accessories__c != null');
                    Map<String, SBQQ__ProductOption__c> existingOptionMap = CustomDataloadAssembler.toBuildProductOptionMap(productOptionMapByProductId.get(product.Id));
                    String[] associatedAccessories = ((String) dataload.Associated_Products_Accessories__c).split(',');
                    
                    CustomDataloadAction.addProductOptionList(dataload, associatedAccessories, productAccessoryMap,
                                                              successedProductFeatureMap, accessoryProductOptionList,
                                                              ACCESSORY_PREFIX, product, ACCESSORY, existingOptionMap);
                }
                dataload.IsProcessed__c = true;
            }
            
            
        }
         
        System.debug('componentProductOptionList.size: '+componentProductOptionList.size());
        if(componentProductOptionList.size() > 0) 
        {
            Database.insert(componentProductOptionList, false);
        }
        
        System.debug('optionProductOptionList.size: '+optionProductOptionList.size());
        if(optionProductOptionList.size() > 0) 
        {
            Database.insert(optionProductOptionList, false);
        }
        
        System.debug('accessoryProductOptionList.size: '+accessoryProductOptionList.size());
        if(accessoryProductOptionList.size() > 0) 
        {
            Database.insert(accessoryProductOptionList, false); 
        }
       
        if(dataloadList.size() > 0) {
            Database.update(dataloadList, false);
        }       
    }
    
}
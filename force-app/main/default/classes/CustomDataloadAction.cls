public class CustomDataloadAction {
    
    public static void setupSuccessedProductFeatureMap(List<SBQQ__ProductFeature__c> productFeatureList, 
                                                       Map<String, SBQQ__ProductFeature__c> successedProductFeatureMap,
                                                       String componentPrefix, String optionPrefix, String accessoryPrefix,
                                                       String component, String option, String accessory) {
        for(SBQQ__ProductFeature__c feature: productFeatureList) {
            if(feature.Id != null){
                if(feature.Name == component) {
                    successedProductFeatureMap.put(componentPrefix + feature.SBQQ__ConfiguredSKU__c, feature);
                }else if(feature.NAME == option) {
                    successedProductFeatureMap.put(optionPrefix + feature.SBQQ__ConfiguredSKU__c, feature);
                }else if(feature.NAME == accessory) {
                    successedProductFeatureMap.put(accessoryPrefix + feature.SBQQ__ConfiguredSKU__c, feature);
                }
            } 
        }
    }

    public static void setupSuccessedFlag(List<Dataload__c> dataloadList, Map<String, Product2> productMap, 
                                        Map<String, PricebookEntry> successedPricebookEntryMap,
                                        Map<String, SBQQ__Cost__c> successedCostMap,
                                        String auPrefix, String nzPrefix 
                                       ) {                     
        for(Dataload__c dataload: dataloadList) {
        	Product2 product = productMap.get(dataload.ProductID__c);
            if(product != null) {
                if(successedPricebookEntryMap.get(auPrefix + product.Id) != null) {
                    dataload.AU_PricebookEntry_Created__c = true;
                }
                if(successedPricebookEntryMap.get(nzPrefix + product.Id) != null) {
                    dataload.NZ_PricebookEntry_Created__c = true;
                }
                if(successedCostMap.get(auPrefix + product.Id) != null) {
                    dataload.AU_Cost_Record_Created__c = true;
                }
                if(successedCostMap.get(nzPrefix + product.Id) != null) {
                    dataload.NZ_Cost_Record_Created__c = true;
                }
                
                /*if(dataload.Product_Type__c == 'ACCESSORY' || dataload.Product_Type__c == 'COMPONENT' || dataload.Product_Type__c == 'OPTION') {
                	dataload.IsProcessed__c = true;
                }*/
            }
        }                                     
    }
    
    public static void addProductOptionList(Dataload__c dataload, String[] associatedProducts, 
                                            Map<String, Product2> associatedProductMap,
                                            Map<String, SBQQ__ProductFeature__c> successedProductFeatureMap,
                                            List<SBQQ__ProductOption__c> productOptionList, String prefix, 
                                            Product2 product, String typeStr, Map<String, SBQQ__ProductOption__c> existingOptionMap) {
        
        for(String associatedProductId: associatedProducts) {
            Product2 associatedProduct = associatedProductMap.get(associatedProductId.trim());
            if(associatedProduct != null) {
                String featureId = successedProductFeatureMap.get(prefix + product.Id) != null? 
                    successedProductFeatureMap.get(prefix + product.Id).Id: null;
                
                if(existingOptionMap.get(associatedProductId.trim()) == null) {
                    productOptionList.add(CustomDataloadAssembler.toBuildProductOption(product, associatedProduct.Id, featureId, typeStr));
                }
            }else {
                switch on  typeStr{ 
                    when 'Components' {
                        dataload.Component_Product_Not_Found__c = true;
                    }
                    when 'Options' {
                        dataload.Option_Product_Not_Found__c = true;
                    }
                    When 'Accessories' {
                        dataload.Accessory_Product_Not_Found__c = true;
                    }
                    when else {
                        System.debug('none type');
                    }
                    	                    	
                }
                
            }
        }
    }
    
    
}
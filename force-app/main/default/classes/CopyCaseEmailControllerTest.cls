@isTest
public class CopyCaseEmailControllerTest {
    
    @testSetup static void setup() {
        Case newCase = new Case();
        newCase.Subject = 'TEST CASE 01';
        newCase.Status = 'New';
        newCase.Origin = 'Email';
        insert newCase;
        
        EmailMessage newEmailMsg = new EmailMessage();
        newEmailMsg.RelatedToId = newCase.Id;
        newEmailMsg.ParentId = newCase.Id;
        newEmailMsg.TextBody = 'test text body';
        newEmailMsg.FromAddress = 'test@test.com';
        newEmailMsg.ToAddress = 'test@test.com';
        newEmailMsg.Incoming = true;
        insert newEmailMsg;
        
        ContentVersion newContent = new ContentVersion();
        newContent.Title = 'test title1';
        newContent.PathOnClient = '.txt';
        newContent.VersionData = Blob.valueOf('test');
        newContent.FirstPublishLocationId = newEmailMsg.Id;
       	insert newContent;
        
        newContent = new ContentVersion();
        newContent.Title = 'test title2';
        newContent.PathOnClient = '.txt';
        newContent.VersionData = Blob.valueOf('test');
        newContent.FirstPublishLocationId = newEmailMsg.Id;
        insert newContent;
        
    }
    
    @isTest static void cloneAllEmailsWithAllAttachmentsTest() {
        Case newCase = new Case();
        newCase.Subject = 'manual create case record';
        newCase.Status = 'New';
        newCase.Origin = 'Email';
        insert newCase;
        
        Case existedCase = [Select Id From Case Where Subject = 'TEST CASE 01'];
        
        List<CopyCaseEmailController.CaseEmailRequest> requestList = new List<CopyCaseEmailController.CaseEmailRequest>();
        CopyCaseEmailController.CaseEmailRequest request = new CopyCaseEmailController.CaseEmailRequest();
        request.ExistedCaseId = existedCase.Id;
        request.CurrentCaseId = newCase.Id;
        requestList.add(request);
        
        Test.startTest();
        CopyCaseEmailController.copyCaseEmailAction(requestList);
        Test.stopTest();
        
    }

}
@IsTest
public class InvokeFlowActionTemplateNoteTest 
{
  static testMethod void InvoiceSinglePaymentTest() 
  {
      Account mAccount = new Account();
      mAccount.Name = 'Test Account';
      insert mAccount;
      
      Opportunity mOpp = New Opportunity();
      mOpp.Name = 'Test Opp';
      mOpp.CloseDate = System.today()+30;
      mOpp.StageName = 'Enquiry';
      insert mOpp;
          
      SBQQ__Quote__c mQuote = new SBQQ__Quote__c();
      mQuote.SBQQ__Account__c = mAccount.Id;
      mQuote.SBQQ__Opportunity2__c = mOpp.Id;
      insert mQuote;
      
      SBQQ__QuoteLineGroup__c mQuoteGroup = new SBQQ__QuoteLineGroup__c();
      mQuoteGroup.SBQQ__Account__c = mAccount.id;
      mQuoteGroup.SBQQ__Quote__c = mQuote.Id;
      insert mQuoteGroup;
      
      String TestText = '*Trimless kits must be Plaster Set into the ceiling prior to Painting; *Concrete box for existing masonry walls to be Rendered/ Plastered and painted; Constant Voltage fittings must be wired in Parallel';
        Test.startTest();
      
      	// Test for case when field existing is NULL
      	InvokeFlowActionTemplateNote.FlowInputs request = new InvokeFlowActionTemplateNote.FlowInputs();
        request.TemplateNotes = TestText;
        request.QuoteLineGroupId = mQuoteGroup.Id;
        
        InvokeFlowActionTemplateNote.AppendTextOnQuoteLineGroup(New InvokeFlowActionTemplateNote.FlowInputs[]{request});
      	
      	// Update Field existing to not be NULL
      	mQuoteGroup.SBQQ__Description__c = '<ul><li>This can be a nice little text description, however I cant prefill it with values.</li><li><br></li></ul>';
      	update mQuoteGroup;
        
        InvokeFlowActionTemplateNote.FlowInputs request2 = new InvokeFlowActionTemplateNote.FlowInputs();
        request2.TemplateNotes = TestText;
        request2.QuoteLineGroupId = mQuoteGroup.Id;
        
        InvokeFlowActionTemplateNote.AppendTextOnQuoteLineGroup(New InvokeFlowActionTemplateNote.FlowInputs[]{request2});
        Test.stopTest();
		
    }

}
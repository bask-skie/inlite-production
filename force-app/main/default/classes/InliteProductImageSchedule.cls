global class InliteProductImageSchedule implements Schedulable {

    global void execute(SchedulableContext cyx) {
        Database.executeBatch(new InliteProductImageBatch(), 10);
    }
}
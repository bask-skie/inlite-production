public class CustomDataloadAssembler {

    public static Id getStandardPricebookId() 
    {
        if(Test.isRunningTest()) 
        {
            return Test.getStandardPricebookId();
        }
        
        Pricebook2 pricebook = getStandardPricebook(); 
        
        return getStandardPricebook() != null? getStandardPricebook().Id: null;
    }
    
    public static Id getPricebookIdByName(String pricebookName) 
    {
        return getPricebookByName(pricebookName) != null? getPricebookByName(pricebookName).Id: null;
    }
    
    private static Pricebook2 getStandardPricebook() 
    {
        List<Pricebook2> pricebookList = [SELECT Id, Name FROM Pricebook2 WHERE IsActive = true AND IsStandard = true LIMIT 1];
        return pricebookList.size() > 0? pricebookList.get(0): null;
    }
    
    private static Pricebook2 getPricebookByName(String pricebookName) 
    {
        
        List<Pricebook2> pricebookList = [SELECT Id, Name FROM Pricebook2 WHERE IsActive = true AND Name =: pricebookName LIMIT 1];
        return pricebookList.size() > 0? pricebookList.get(0): null;
    }
    
    public static PricebookEntry toBuildPricebookEntry(Id pricebook2Id, Decimal unitPrice, Id product2Id) 
    {
        PricebookEntry pricebookEntry = new PricebookEntry();
        pricebookEntry.IsActive = true;
        pricebookEntry.Pricebook2Id = pricebook2Id;
        pricebookEntry.UnitPrice = unitPrice;
        pricebookEntry.Product2Id = product2Id;
        
        return pricebookEntry;
    }
    
    public static SBQQ__Cost__c toBuildCost(Decimal unitCost, Id product2Id, String country) 
    {
        SBQQ__Cost__c cost = new SBQQ__Cost__c();
        cost.SBQQ__Active__c = true;
        cost.SBQQ__UnitCost__c = unitCost;
        cost.SBQQ__Product__c = product2Id;
        cost.Country__c = country;
        
        return cost;
    }
    
    public static SBQQ__ProductFeature__c toBuildProductFeature(String name, Integer min, Integer max, Integer numb, Id Product2Id) 
    {
        SBQQ__ProductFeature__c feature = new SBQQ__ProductFeature__c();
        feature.Name = name;
        feature.SBQQ__MinOptionCount__c = min;
        feature.SBQQ__MaxOptionCount__c = max;
        feature.SBQQ__Number__c = numb;
        feature.SBQQ__ConfiguredSKU__c = Product2Id;
        
        return feature;
    }
    
    public static SBQQ__ConfigurationRule__c toBuildConfigurationRule(Id productRuleId, Id product2Id) 
    {
        SBQQ__ConfigurationRule__c config = new SBQQ__ConfigurationRule__c();
        config.SBQQ__Active__c = true;
        config.SBQQ__ProductRule__c = productRuleId;
        config.SBQQ__Product__c = product2Id;
        
        return config;
    }
    
    public static Id getProductRuleIdByName(String name) 
    {
        return getProductRuleByName(name) != null? getProductRuleByName(name).Id: null;
    }
    
    private static SBQQ__ProductRule__c getProductRuleByName(String name) 
    {
        List<SBQQ__ProductRule__c> productRuleList = [SELECT Id, Name FROM SBQQ__ProductRule__c WHERE Name =: name LIMIT 1];
        return productRuleList.size() > 0? productRuleList.get(0): null;
    }
    
    public static SBQQ__ProductOption__c toBuildProductOption(Product2 mainProduct, Id subProductId, Id productFeatureId, String typeString) 
    {
        SBQQ__ProductOption__c option = new SBQQ__ProductOption__c();
        option.SBQQ__ConfiguredSKU__c = mainProduct.Id;
        option.SBQQ__OptionalSKU__c = subProductId;
        option.SBQQ__Feature__c = productFeatureId;
        option.SBQQ__Number__c = 1;
        //new changes is to set type as 'Component' for every product option created.
        //option.SBQQ__Type__c =  mainProduct.Product_Type__c != null && (mainProduct.Product_Type__c == 'PROFILE' || mainProduct.Product_Type__c == 'TRACK')? 'Accessory': 'Component';
        option.SBQQ__Type__c = 'Component';
        option.SBQQ__QuantityEditable__c = true;
            
        return option;
    }
    
    public static Map<String, PricebookEntry> toBuildSuccessedPricebookEntryMap(String stdPricebookId, String nzPricebookId, String AU_PREFIX, String NZ_PREFIX, 
                                                                                List<PricebookEntry> pricebookEntryList, Map<String, PricebookEntry> successedPricebookEntryMap) 
    {
        for(PricebookEntry pricebookEntry :pricebookEntryList) 
        {
            if(pricebookEntry.Id != null)
            {
                if(pricebookEntry.Pricebook2Id == stdPricebookId) 
                {
                    successedPricebookEntryMap.put(AU_PREFIX + pricebookEntry.Product2Id, pricebookEntry);
                }
                else if(pricebookEntry.Pricebook2Id == nzPricebookId) 
                {
                    successedPricebookEntryMap.put(NZ_PREFIX + pricebookEntry.Product2Id, pricebookEntry);
                }  
            }
            
        }
        
        return successedPricebookEntryMap;
    }
    
    public static Map<String, SBQQ__Cost__c> toBuildSuccessedCostMap(String AU_PREFIX, String NZ_PREFIX, List<SBQQ__Cost__c> costList, Map<String, SBQQ__Cost__c> successedCostMap) 
    {
        for(SBQQ__Cost__c cost :costList) 
        {
            if(cost.id != null){
                if(cost.Country__c == 'AU') 
                {
                    successedCostMap.put(AU_PREFIX + cost.SBQQ__Product__c, cost);
                }
                else if(cost.Country__c == 'NZ') 
                {
                    successedCostMap.put(NZ_PREFIX + cost.SBQQ__Product__c, cost);
                }
                
            }
        }
       
        return successedCostMap;
    }
    
    public static Map<String, SBQQ__ProductOption__c> toBuildProductOptionMap(List<SBQQ__ProductOption__c> productOptionList)
    {
        Map<String, SBQQ__ProductOption__c> productOptionMap = new Map<String, SBQQ__ProductOption__c>();
        if(productOptionList != null) 
        {
            for(SBQQ__ProductOption__c option : productOptionList) 
            {
                if(option.SBQQ__OptionalSKU__r.ProductID__c != null) 
                {
                    productOptionMap.put(option.SBQQ__OptionalSKU__r.ProductID__c, option);
                }
            }
        }
        
        return productOptionMap;
    }
    
}
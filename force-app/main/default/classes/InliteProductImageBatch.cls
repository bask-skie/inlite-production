global class InliteProductImageBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id, ProductID__c, Images_Retrieved__c, HttpStatus_Code__c, '+
            		   'App_Image_1__c, App_Image_2__c, App_Image_3__c, App_Image_4__c,  '+
            		   'App_Image_5__c, App_Image_6__c, Product_Hero__c '+
            		   'FROM Product2 '+
            		   'WHERE Images_Retrieved__c <> true '+
            		   'AND ProductID__c <> null ';
           
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scopes){
        List<Product2> products = (List<Product2>) scopes;
        List<Product2> updProducts = new List<Product2>();
        for(Product2 product: products) {
            InliteProductImageController.callout(product);
            updProducts.add(product);
        }
        update updProducts;
        
    }
    
    global void finish(Database.BatchableContext BC){
    }
}
global class CustomDataloadSchedule implements Schedulable {

    global void execute(SchedulableContext cyx) {
        Database.executeBatch(new CustomDataloadBatch(), 5);
    }
}
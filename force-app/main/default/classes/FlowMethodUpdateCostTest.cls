@IsTest
public class FlowMethodUpdateCostTest 
{
    static testMethod void UpdateCostTest() 
    {
        Account acc = new Account();
        acc.Name = 'ACC TEST';
        acc.Account_Grade__c = '3';
        acc.Type = 'Specifier';
        acc.Specifier_Type__c = 'End User';
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'OPP TEST';
        opp.StageName = 'Quoting';
        opp.CloseDate = System.today() + 1;
        insert opp;

        SBQQ__Quote__c mQuote = new SBQQ__Quote__c();
        mQuote.OwnerId =  UserInfo.getUserId();
        mQuote.SBQQ__Account__c = acc.Id;
        mQuote.SBQQ__Opportunity2__c = opp.Id;
        insert mQuote;
        
        SBQQ__QuoteLineGroup__c quoteGroup = new SBQQ__QuoteLineGroup__c();
        quoteGroup.Name = 'Group Test';
        quoteGroup.SBQQ__Number__c = 1;
        quoteGroup.SBQQ__Quote__c = mQuote.Id;
        quoteGroup.SBQQ__ListTotal__c = 10;
        quoteGroup.SBQQ__CustomerTotal__c = 10;
        quoteGroup.SBQQ__NetTotal__c = 10;
        insert quoteGroup;
        
        SBQQ__QuoteLineGroup__c quoteGroup2 = new SBQQ__QuoteLineGroup__c();
        quoteGroup2.Name = 'Group Test2';
        quoteGroup2.SBQQ__Number__c = 1;
        quoteGroup2.SBQQ__Quote__c = mQuote.Id;
        quoteGroup2.SBQQ__ListTotal__c = 10;
        quoteGroup2.SBQQ__CustomerTotal__c = 10;
        quoteGroup2.SBQQ__NetTotal__c = 10;
        insert quoteGroup2;
        
        Product2 mProduct = new Product2();
        mProduct.Name = 'Product Test';
        insert mProduct;
        
        Product2 mProduct2 = new Product2();
        mProduct2.Name = 'Product Test2';
        insert mProduct2;
        
        
        SBQQ__Cost__c mCost = new SBQQ__Cost__c();
        mCost.SBQQ__UnitCost__c = 15;
        mCost.SBQQ__Product__c = mProduct.id;
        insert mCost;
            
        SBQQ__QuoteLine__c mQuoteLine = new SBQQ__QuoteLine__c();
		mQuoteLine.SBQQ__Quote__c = mQuote.Id;
        mQuoteLine.SBQQ__Group__c = quoteGroup.Id;
        mQuoteLine.SBQQ__Product__c = mProduct.Id;
        mQuoteLine.SBQQ__Cost__c = mCost.Id;
        mQuoteLine.SBQQ__NetPrice__c = 20;
        insert mQuoteLine;
        
        SBQQ__QuoteLine__c mQuoteLine2 = new SBQQ__QuoteLine__c();
		mQuoteLine2.SBQQ__Quote__c = mQuote.Id;
        mQuoteLine2.SBQQ__Group__c = quoteGroup.Id;
        mQuoteLine2.SBQQ__Product__c = mProduct.Id;
        mQuoteLine2.SBQQ__Cost__c = mCost.Id;
        mQuoteLine2.SBQQ__NetPrice__c = 30;
        mQuoteLine2.SBQQ__MarkupAmount__c = 10;
        insert mQuoteLine2;
        
        Test.startTest();
            List<Id> mQuoteIdList = new List<Id>();
            mQuoteIdList.add(mQuote.id);
            FlowMethodUpdateCost.updateCost(mQuoteIdList);
        Test.stopTest();
    }
    static testMethod void UpdateCostWithoutCostTest() 
    {
        Account acc = new Account();
        acc.Name = 'ACC TEST';
        acc.Account_Grade__c = '3';
        acc.Type = 'Specifier';
        acc.Specifier_Type__c = 'End User';
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'OPP TEST';
        opp.StageName = 'Quoting';
        opp.CloseDate = System.today() + 1;
        insert opp;

        SBQQ__Quote__c mQuote = new SBQQ__Quote__c();
        mQuote.OwnerId =  UserInfo.getUserId();
        mQuote.SBQQ__Account__c = acc.Id;
        mQuote.SBQQ__Opportunity2__c = opp.Id;
        insert mQuote;
        
        SBQQ__QuoteLineGroup__c quoteGroup = new SBQQ__QuoteLineGroup__c();
        quoteGroup.Name = 'Group Test';
        quoteGroup.SBQQ__Number__c = 1;
        quoteGroup.SBQQ__Quote__c = mQuote.Id;
        quoteGroup.SBQQ__ListTotal__c = 10;
        quoteGroup.SBQQ__CustomerTotal__c = 10;
        quoteGroup.SBQQ__NetTotal__c = 10;
        insert quoteGroup;
        
        SBQQ__QuoteLineGroup__c quoteGroup2 = new SBQQ__QuoteLineGroup__c();
        quoteGroup2.Name = 'Group Test2';
        quoteGroup2.SBQQ__Number__c = 1;
        quoteGroup2.SBQQ__Quote__c = mQuote.Id;
        quoteGroup2.SBQQ__ListTotal__c = 10;
        quoteGroup2.SBQQ__CustomerTotal__c = 10;
        quoteGroup2.SBQQ__NetTotal__c = 10;
        insert quoteGroup2;
        
        Product2 mProduct = new Product2();
        mProduct.Name = 'Product Test';
        insert mProduct;
        
        Product2 mProduct2 = new Product2();
        mProduct2.Name = 'Product Test2';
        insert mProduct2;
        
        
        SBQQ__Cost__c mCost = new SBQQ__Cost__c();
        mCost.SBQQ__UnitCost__c = 15;
        mCost.SBQQ__Product__c = mProduct.id;
        insert mCost;
            
        SBQQ__QuoteLine__c mQuoteLine = new SBQQ__QuoteLine__c();
		mQuoteLine.SBQQ__Quote__c = mQuote.Id;
        mQuoteLine.SBQQ__Group__c = quoteGroup.Id;
        mQuoteLine.SBQQ__Product__c = mProduct.Id;
        mQuoteLine.SBQQ__Cost__c = mCost.Id;
        mQuoteLine.SBQQ__NetPrice__c = 20;
        insert mQuoteLine;
        
        SBQQ__QuoteLine__c mQuoteLine2 = new SBQQ__QuoteLine__c();
		mQuoteLine2.SBQQ__Quote__c = mQuote.Id;
        mQuoteLine2.SBQQ__Group__c = quoteGroup.Id;
        mQuoteLine2.SBQQ__Product__c = mProduct2.Id;
        //mQuoteLine2.SBQQ__Cost__c = mCost.Id;
        mQuoteLine2.SBQQ__NetPrice__c = 30;
        insert mQuoteLine2;
        
        Test.startTest();
            List<Id> mQuoteIdList = new List<Id>();
            mQuoteIdList.add(mQuote.id);
            FlowMethodUpdateCost.updateCost(mQuoteIdList);
        Test.stopTest();
    }

}
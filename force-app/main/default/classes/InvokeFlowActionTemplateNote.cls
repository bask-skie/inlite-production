public class InvokeFlowActionTemplateNote 
{
	@InvocableMethod(label='Action Invoice Template Note, Add the Template note to the QuoteLineGroup Description')
    public static void AppendTextOnQuoteLineGroup(List<FlowInputs> requests) 
    {
        SBQQ__QuoteLineGroup__c mQuoteGroup = [SELECT Id, SBQQ__Description__c 
                                               FROM SBQQ__QuoteLineGroup__c
                                               WHERE Id =:requests[0].QuoteLineGroupId];

        If(mQuoteGroup!=NULL)
        {
            string strTemplateNotes = requests[0].TemplateNotes;
            String TemplateNotes;
            If(strTemplateNotes!=NULL)
            {
                String[] strTemplateNotesArray = strTemplateNotes.split(';');
                for (String i : strTemplateNotesArray) 
                {
                    If(TemplateNotes == NULL)
                    {
                        TemplateNotes = '<li>' + i + '</li>';
                    }
                    else
                    {
                        TemplateNotes = TemplateNotes + '<li>' + i + '</li>';
                    }
                }
            }
            
            If(mQuoteGroup.SBQQ__Description__c!=NULL)
            {
                mQuoteGroup.SBQQ__Description__c = mQuoteGroup.SBQQ__Description__c + '<ul>'+ TemplateNotes + '</ul>';
            }
            else
            {
                mQuoteGroup.SBQQ__Description__c = '<ul>'+ TemplateNotes + '</ul>';
            }
            update mQuoteGroup;
        }
      }
    
    //input details that comes to apex from flow
    public class FlowInputs
    {
    	@InvocableVariable
        public String TemplateNotes;
        
        @InvocableVariable(required=true)
        public Id QuoteLineGroupId;
        
    }
    
    //output details which goes from apex to flow
    /*public class FlowOutputs{
        
        @InvocableVariable
        public String accountPreviousName;
        
        @InvocableVariable
        public String DMLResult;
    }*/
}
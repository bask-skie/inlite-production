public class QuoteLineGroupController {
    public static void createQuoteLineGroup(List<SBQQ__QuoteLineGroup__c> quoteGroupList){
        System.debug('quoteGroupList ===>' + quoteGroupList);
        Set<String> quoteIdSet = new Set<String>();
        Map<String, String> readOnlyQuoteMap = new Map<String, String>();
        List<Read_Only_Quote_Group__c> readOnlyQuoteGroupList = new List<Read_Only_Quote_Group__c>();
        Map<String, Read_Only_Quote_Group__c> readOnlyQuoteGroupMap = new Map<String, Read_Only_Quote_Group__c>();
        
        for(SBQQ__QuoteLineGroup__c quoteGroup : quoteGroupList){
            quoteIdSet.add(quoteGroup.SBQQ__Quote__c);
        }
                
		List<SBQQ__Quote__c> quoteList = [Select Id, Read_Only_Quote__c from SBQQ__Quote__c Where Id in : quoteIdSet];
        for(SBQQ__Quote__c quote : quoteList){
            readOnlyQuoteMap.put(quote.Id, quote.Read_Only_Quote__c);
        }
        
		for(SBQQ__QuoteLineGroup__c quoteLineGroup: quoteGroupList){
			Read_Only_Quote_Group__c readOnlyQuoteGroup = new Read_Only_Quote_Group__c();
            readOnlyQuoteGroup.Quote_Line_Group__c = quoteLineGroup.id;
            readOnlyQuoteGroup.Read_Only_Quote__c = readOnlyQuoteMap.get(quoteLineGroup.SBQQ__Quote__c);
            readOnlyQuoteGroupList.add(readOnlyQuoteGroup);
            readOnlyQuoteGroupMap.put(quoteLineGroup.Id, readOnlyQuoteGroup);
		}
        
        if(readOnlyQuoteGroupList.size() > 0){
            insert readOnlyQuoteGroupList;
            
            List<SBQQ__QuoteLineGroup__c> newQuoteGroupList= [Select Id, Read_Only_Quote_Group__c From SBQQ__QuoteLineGroup__c Where Id In : quoteGroupList];

            for(SBQQ__QuoteLineGroup__c quoteGroup: newQuoteGroupList){
                if(readOnlyQuoteGroupMap.get(quoteGroup.Id) != null){
                    quoteGroup.Read_Only_Quote_Group__c = readOnlyQuoteGroupMap.get(quoteGroup.Id).Id;
                }
            }
            
            if(newQuoteGroupList.size() > 0) {
                update newQuoteGroupList;
            }
        }
    }
    
	public static void deleteQuoteLineGroup(List<SBQQ__QuoteLineGroup__c> quoteGroupList){
        Set<String> readOnlyQuoteGroupId = new Set<String>();
        for(SBQQ__QuoteLineGroup__c quoteGroup: quoteGroupList){
            readOnlyQuoteGroupId.add(quoteGroup.Read_Only_Quote_Group__c);
        }
        List<Read_Only_Quote_Group__c> readOnlyQuoteGroupList = [Select Id From Read_Only_Quote_Group__c Where Id in: readOnlyQuoteGroupId];
        delete readOnlyQuoteGroupList;
    }
}
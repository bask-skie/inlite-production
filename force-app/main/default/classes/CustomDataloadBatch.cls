global class CustomDataloadBatch implements Database.Batchable<sObject>{

    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id, Name, Existing_Product__c, No_Existing_Product__c, ProductID__c, Product__c, Product_Type__c, '+
            		   'IsProcessed__c, Trade_Price__c, Cost_Price__c, NZ_Trade_Price__c, NZ_Cost_Price__c, '+
             		   'Associated_Products_Components__c, Associated_Products_Options__c, Associated_Products_Accessories__c, '+
            		   'Component_Product_Not_Found__c, Option_Product_Not_Found__c, Accessory_Product_Not_Found__c '+
            		   'FROM Dataload__c '+
            		   'WHERE IsProcessed__c <> true ';
           
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scopes){
        
        CustomDataloadController.manageCustomDataload(scopes);
    }
    
    global void finish(Database.BatchableContext BC){
    }
    
}
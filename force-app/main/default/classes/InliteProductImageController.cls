public class InliteProductImageController {
    
    public static void callout(Product2 productObj) {
        try{
            
            System.debug('Id of Product: '+productObj.Id);
            Product_Image_Setting__c setting = Product_Image_Setting__c.getOrgDefaults();
            String endpoint = setting.Product_Detail_Endpoint__c;
            String key = setting.API_Key__c;
            
            String productID = EncodingUtil.urlEncode(productObj.productID__c, 'UTF-8');
            String productEndpoint = endpoint+'/'+productID;
            System.debug('productEndpoint: '+productEndpoint);
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setHeader('api_key', key);
            request.setEndpoint(productEndpoint);
            request.setMethod('GET');
            request.setTimeout(30000);
            HttpResponse response = http.send(request);
            Integer calloutLimit = Limits.getCallouts();
            
            System.debug('response.getStatusCode(): '+response.getStatusCode());
            if(response.getStatusCode() != 500 && response.getHeader('Location') != null) {
                if(response.getStatusCode() >= 300 && response.getStatusCode() <= 307 && response.getStatusCode() != 306){
                    System.debug('response.getHeader("Location"): '+response.getHeader('Location'));
                    request.setEndpoint(response.getHeader('Location'));
                    response = new Http().send(request);
                }
            }
            
            String statusCode =  String.valueOf(response.getStatusCode());
            if(response.getStatusCode() == 200) {
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                System.debug('results: '+results);
                
                List<Object> products = (List<Object>)results.get('products');
                if(products != null) {
                    System.debug('product list != null, size='+products.size());
                    
                    manageProductList(products, productObj);
                    
                }else {
                    System.debug('product object');
                    manageProductObject(results, productObj);
                }
                
                productObj.HttpStatus_Code__c = statusCode;
            }else {
                productObj.HttpStatus_Code__c = statusCode;
            }
            
            productObj.Images_Retrieved__c = true;
            
            System.debug('Limits.getCallouts(): '+Limits.getCallouts());
            
        }catch (Exception e){
            productObj.Images_Retrieved__c = true;
            productObj.HttpStatus_Code__c = e.getMessage();
            System.debug('Exception: '+e.getMessage());
        }
    }
    
    public static void manageProductList(List<Object> products, Product2 productObj) {
        for(Object product: products) {
            
            Map<String, Object> productWrapper = (Map<String, Object>) product;
            String inlite_code = (String) productWrapper.get('inlite_code');
            if(inlite_code != null && inlite_code.equals(productObj.productID__c)) {
                System.debug('found matched inlite_code with productID__c: '+productObj.productID__c);
                manageProductObject(productWrapper, productObj);
            }
        }
    }
    
    public static void manageProductObject(Map<String, Object> product, Product2 productObj){
        List<Object> application_images = (List<Object>) product.get('application_images');
        
        if(application_images != null && application_images.size() > 0) {
            Integer index = 0;
            
            for(Object application_image: application_images) {
                
                Map<String, Object> image = (Map<String, Object>)application_image;
                Map<String, Object> sizes = (Map<String, Object>)image.get('sizes');
                String medium_image_url = (String) sizes.get('medium');
                System.debug('(application_image) medium_url: '+medium_image_url);
                
                if(medium_image_url != null) {
                    if(index == 0) {
                        productObj.App_Image_1__c = medium_image_url;
                    }else if(index == 1) {
                        productObj.App_Image_2__c = medium_image_url;
                    }else if(index == 2) {
                        productObj.App_Image_3__c = medium_image_url;
                    }else if(index == 3) {
                        productObj.App_Image_4__c = medium_image_url;
                    }else if(index == 4) {
                        productObj.App_Image_5__c = medium_image_url;
                    }else if(index == 5) {
                        productObj.App_Image_6__c = medium_image_url;
                    }else {
                        break;
                    }
                }
                
                index++; 
            }
        }
        
        Map<String, Object> productHero = (Map<String, Object>) product.get('product_hero');
        if(productHero != null) {
            String medium_image_url = (String) productHero.get('medium');
            System.debug('(product_hero_image) medium_url: '+ medium_image_url);
            productObj.Product_Hero__c = medium_image_url;
        }
        
    }
    
}
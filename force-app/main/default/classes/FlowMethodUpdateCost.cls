public class FlowMethodUpdateCost 
{
    @InvocableMethod(label='Update Quoteline and Quote')
    public static void updateCost(List<Id> QuoteIds) 
    {
        If(QuoteIds.isEmpty()==FALSE)
        {
            List<SBQQ__QuoteLine__c> QuoteLineList = [SELECT Id, SBQQ__Quote__c, SBQQ__Cost__c, SBQQ__MarkupRate__c, SBQQ__MarkupAmount__c, 
                                                      New_Markup__c, SBQQ__Cost__r.SBQQ__UnitCost__c, SBQQ__UnitCost__c, Total_Cost__c 
                                                      FROM SBQQ__QuoteLine__c 
                                                      WHERE SBQQ__Quote__c =:QuoteIds[0] ];
            
            List<SBQQ__QuoteLine__c> updateQuoteLineList = new List<SBQQ__QuoteLine__c>();
            For(SBQQ__QuoteLine__c tmpQuoteLine:QuoteLineList)
            {
                If((tmpQuoteLine.SBQQ__Cost__c!=NULL)&&(tmpQuoteLine.SBQQ__MarkupAmount__c==NULL))
                {
                    tmpQuoteLine.SBQQ__UnitCost__c = tmpQuoteLine.SBQQ__Cost__r.SBQQ__UnitCost__c;
                    tmpQuoteLine.SBQQ__MarkupRate__c = tmpQuoteLine.New_Markup__c;
                    updateQuoteLineList.add(tmpQuoteLine);
                }
                else If((tmpQuoteLine.SBQQ__Cost__c!=NULL)&&(tmpQuoteLine.SBQQ__MarkupAmount__c!=NULL))
                {
                    tmpQuoteLine.SBQQ__UnitCost__c = tmpQuoteLine.SBQQ__Cost__r.SBQQ__UnitCost__c;
                    updateQuoteLineList.add(tmpQuoteLine);
                }
                else If((tmpQuoteLine.SBQQ__Cost__c==NULL)&&(tmpQuoteLine.SBQQ__MarkupAmount__c==NULL))
                {
                    tmpQuoteLine.SBQQ__MarkupRate__c = tmpQuoteLine.New_Markup__c;
                    updateQuoteLineList.add(tmpQuoteLine);
                }
            }
            
            If(updateQuoteLineList.isEmpty()==FALSE)
            {
                Update updateQuoteLineList;
            }
            
            
            List<SBQQ__QuoteLineGroup__c> QuoteLineGroupList = [SELECT Id, SBQQ__Quote__c, Total_Cost__c
                                                                FROM SBQQ__QuoteLineGroup__c 
                                                                WHERE SBQQ__Quote__c =:QuoteIds[0] ];
            List<SBQQ__QuoteLineGroup__c> updateQuoteLineGroupList = new List<SBQQ__QuoteLineGroup__c>();
            Set<Id> GroupIds = new Set<Id>();
            If(QuoteLineGroupList.isEmpty()==FALSE)
            {
                For(SBQQ__QuoteLineGroup__c mGroup:QuoteLineGroupList)
                {
                    GroupIds.add(mGroup.Id);
                }
                
                List<SBQQ__QuoteLine__c> QuoteLineFromGroupList = [SELECT Id, SBQQ__UnitCost__c, SBQQ__Group__c, Total_Cost__c 
                                                                   FROM SBQQ__QuoteLine__c  
                                                                   WHERE SBQQ__Group__c IN:GroupIds];
                Map<ID, Decimal> mapGroupCost = new Map<ID, Decimal>();
                If(QuoteLineFromGroupList.isEmpty()==FALSE)
                {
                    For(SBQQ__QuoteLine__c tmp:QuoteLineFromGroupList)
                    {
                        If(mapGroupCost.containsKey(tmp.SBQQ__Group__c)&&(tmp.Total_Cost__c!=NULL))
                        {
                            Decimal val = mapGroupCost.get(tmp.SBQQ__Group__c) + tmp.Total_Cost__c;
                            mapGroupCost.put(tmp.SBQQ__Group__c,val);
                        }
                        else if(tmp.Total_Cost__c!=NULL)
                        {
                            mapGroupCost.put(tmp.SBQQ__Group__c,tmp.Total_Cost__c);
                        }
                    }
                    
                    For(SBQQ__QuoteLineGroup__c mGroup:QuoteLineGroupList)
                    {
                        If(mapGroupCost.containsKey(mGroup.Id))
                        {
                            mGroup.Total_Cost__c = mapGroupCost.get(mGroup.Id);
                            updateQuoteLineGroupList.add(mGroup);
                        }
                    }
                }
            }
            
            If(updateQuoteLineGroupList.isEmpty()==FALSE)
            {
                update updateQuoteLineGroupList;
            }
            
            
            AggregateResult[] groupResults = [SELECT SUM(Total_Cost__c)totalcost FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c =:QuoteIds[0] ];
            If(groupResults[0].get('totalcost')!=NULL)
            {
                SBQQ__Quote__c mQuote = new SBQQ__Quote__c(id = QuoteIds[0] );
                mQuote.Total_Cost__c = (Decimal)groupResults[0].get('totalcost');
                update mQuote;
            }
        }
      }
}
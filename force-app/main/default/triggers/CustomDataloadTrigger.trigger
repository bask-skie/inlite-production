trigger CustomDataloadTrigger on Dataload__c (before insert, after insert) {

    if(Trigger.isInsert) {
        if(Trigger.isBefore) {
            CustomDataloadController.populateExistingProducts(Trigger.New);
        }
        
        if(Trigger.isAfter) {
        	CustomDataloadController.createNewProducts(Trigger.New);
        }
    }
    
    
}
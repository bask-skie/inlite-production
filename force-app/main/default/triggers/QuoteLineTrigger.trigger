trigger QuoteLineTrigger on SBQQ__QuoteLine__c (after insert, after update, before delete, before update) {
    if(Trigger.IsAfter) {
        if(Trigger.IsInsert) {
            List<SBQQ__QuoteLine__c> insertQuoteLineList = new List<SBQQ__QuoteLine__c>();
            Set<String> quoteIdSet = new Set<String>();
            for(SBQQ__QuoteLine__c quoteLine : Trigger.New){
                quoteIdSet.add(quoteLine.SBQQ__Quote__c);
            }
            List<SBQQ__Quote__c> quoteList = [Select Id, Read_Only_Quote__c From SBQQ__Quote__c Where Id in :quoteIdSet];
            Map<String, String> quoteMap = new Map<String, String>();
            for(SBQQ__Quote__c quote : quoteList){
                if(quote.Read_Only_Quote__c != null){
                    quoteMap.put(quote.Id, quote.Read_Only_Quote__c);
                }
            }
            for(SBQQ__QuoteLine__c quoteLine : Trigger.New){
                if(quoteMap.get(quoteLine.SBQQ__Quote__c) != null){
                    insertQuoteLineList.add(quoteLine);
                }
            }
            QuoteLineController.createQuoteLine(insertQuoteLineList);
        }
        
        if(Trigger.IsUpdate) {
            Map<String, SBQQ__QuoteLine__c> mapOldQuoteLine = new Map<String, SBQQ__QuoteLine__c>();
            Map<String, SBQQ__QuoteLine__c> mapNewQuoteLine = new Map<String, SBQQ__QuoteLine__c>();
            List<SBQQ__QuoteLine__c> updQuoteLineList = new List<SBQQ__QuoteLine__c>();
            
            for(SBQQ__QuoteLine__c quoteLine : Trigger.Old){
                mapOldQuoteLine.put(quoteLine.Id, quoteLine);
            }
            for(SBQQ__QuoteLine__c quoteLine : Trigger.New){
                mapNewQuoteLine.put(quoteLine.Id, quoteLine);
            }
            for(SBQQ__QuoteLine__c quoteLine : Trigger.New){
                
                SBQQ__QuoteLine__c oldQuoteLine = mapOldQuoteLine.get(quoteLine.Id);
                SBQQ__QuoteLine__c newQuoteLine = mapNewQuoteLine.get(quoteLine.Id);
                if (oldQuoteLine.SBQQ__Group__c != newQuoteLine.SBQQ__Group__c){
                    
                    updQuoteLineList.add(quoteLine);
                }
            }
            
            if(updQuoteLineList.size() > 0) {
                QuoteLineController.updateQuoteLine(updQuoteLineList);
            }
        }
    }
    
    if(Trigger.IsBefore) {
        if(Trigger.IsDelete) {
            QuoteLineController.deleteQuoteLine(Trigger.Old);
        }
        
        if(Trigger.IsUpdate) {
            QuoteLineController.checkingRoqlBeforeUpdate(Trigger.New);

        }
    }
}
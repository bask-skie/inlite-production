trigger QuoteLineGroupTrigger on SBQQ__QuoteLineGroup__c (after insert, before delete) {
    if(Trigger.IsAfter) {
        if(Trigger.IsInsert) {
            List<SBQQ__QuoteLineGroup__c> insertQLGList = new List<SBQQ__QuoteLineGroup__c>();
            Set<String> quoteIdSet = new Set<String>();
            for(SBQQ__QuoteLineGroup__c qlg : Trigger.New){
                quoteIdSet.add(qlg.SBQQ__Quote__c);
            }
            List<SBQQ__Quote__c> quoteList = [Select Id, Read_Only_Quote__c From SBQQ__Quote__c Where Id in :quoteIdSet];
            Map<String, String> quoteMap = new Map<String, String>();
            for(SBQQ__Quote__c quote : quoteList){
                if(quote.Read_Only_Quote__c != null){
                    quoteMap.put(quote.Id, quote.Read_Only_Quote__c);
                }
            }
            for(SBQQ__QuoteLineGroup__c qlg : Trigger.New){
                if(quoteMap.get(qlg.SBQQ__Quote__c) != null){
                    insertQLGList.add(qlg);
                }
            }
            QuoteLineGroupController.createQuoteLineGroup(insertQLGList);
        }
    }
    
    if(Trigger.IsBefore) {
        if(Trigger.IsDelete) {
            QuoteLineGroupController.deleteQuoteLineGroup(Trigger.Old);
        }
    }
}